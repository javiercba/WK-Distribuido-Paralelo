# Algoritmo WK - Versión para GPU:Tegra K1, instrumentada para comunicación con ARDUINO

El proposito de esta versión es la medición del consumo de Potencia de Wk_cuda7. 
Se utiliza un hardware propio: Amperímetro basando en ARDUINO UNO y el Sensor 
de efecto Hall: ACS712.  

En esta versión se instrumenta la interpolación de Stolt.  

El algoritmo WK fue Creado en 1991 por Cafforio, Prati y Rocca. Es uno de los
algoritmos actuales más precisos comparado con RDA (Range Doppler Algorithm) y
CSA (Chirp Scaling Algorithm), realiza la focalización de la imagen SAR 
(randar de apertura sintetica) trabajando íntegramente en el dominio bidimensional 
de las frecuencias y de allí su nombre, W:frecuencia en Rango y K: frecuencia en acimut. 
Sus principales ventajas es que puede manejar grandes aperturas sintéticas 
o elevados ángulos de Squint. 

## Descripción del algoritmo WK:

 Entrada: datos crudos simulados SAR

* ifftshit
* fft2D
* iffshift
* compresion gruesa: RFM
* compresion diferencial: Interpolacion de Stolt (instrumentada para ARDUINO)
* ifft2D

Salida: Imagen focalizada

## Compilacion:

```
nvcc -arch=sm_32 wk_cuda7_ardu_2018.cu -o wk_cuda7_ardu_6144 -lcufft -DNslow=6144 -DNfast=6144 -DLAB=1 -DTIME=0 -DWRITE=0
```


## Autor

* **javier nicolas uranga**: javiercba@gmail.com

## Licencia

Este proyecto esta bajo lincencia GPLv3
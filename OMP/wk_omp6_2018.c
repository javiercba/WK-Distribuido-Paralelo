//    wk_omp6_2018.c: 
//    Algoritmo WK, version para CPU paralela utilizando OPENMP

//    Copyright (C) 2018  javier nicolas uranga

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


//mail de contacto: javiercba@gmail.com


//19/01/2018
//gcc wk_omp6_2018.c -lfftw3_omp -lfftw3 -lm -fopenmp -o wk_omp6_t4_6144 -DNslow=6144 -DNfast=6144 -DTHREADS=4 -DTIME=0 -DLAB=1 -DWRITE=0


#include <stdlib.h>
#include <stdio.h>

#include <complex.h> //incluir antes de fftw3
#include <fftw3.h>

#include <math.h>
#include <omp.h>
#include <sys/time.h> 


#define Fc 9.4e9
#define C 3.0e8
#define V 250.0

#define R0 27000
#define Ks 10.0e12

//#define Fslow 600
#define Ffast 120.0e6
#define Fslow  6.0e2 


#ifndef Nslow
#define Nslow 6144
#endif

#ifndef Nfast
#define Nfast 6144
#endif

#define Ni 4

#ifndef LAB
#define LAB 0 
#endif

#ifndef WRITE
#define WRITE 1
#endif

#ifndef TIME
#define TIME 1
#endif

#define PI 3.1416

#define MAX(a,b)((a>b)?a:b)

//-------omp---------------

#define CHUNKSIZE 1

#ifndef THREADS
#define THREADS 4
#endif

//----------------------------------------------- kerneles ------------------------------------------------

double dFc;
double dRfmC3;
double dRfmC2;
double dRfmC1;

double dFfast;

int dLim;

double sinc_func(double x)
{
    return sin(PI*x+ 1e-32)/(PI*x+ 1e-32);
}



//---------------------------fft------------------------------------------------

//unicamente matrices cuadradas y pares
void fftshift(double complex *d_matrix, int filas, int cols)
{
	int fila, col;
	double complex aux;
	#pragma omp parallel shared(d_matrix, filas, cols) private(fila,col, aux)
        {

	
		int tid = omp_get_thread_num();
		printf("debug: fftshift, thread_id:%d\n",tid);

		#pragma omp for schedule (static, CHUNKSIZE)
		for (fila=0; fila<filas; fila++)
		{
		   for(col=0; col<cols; col++)
		   {
				if(fila<filas/2)
				{
					if(col<cols/2)
					{
						aux = d_matrix[fila*cols + col];
						d_matrix[fila*cols + col] = d_matrix[(fila+(filas/2))*cols + (col+(cols/2))];
						d_matrix[(fila+(filas/2))*cols + (col+(cols/2))] = aux;
					}
					else
					{
						aux = d_matrix[fila*cols + col];
						d_matrix[fila*cols + col] = d_matrix[(fila+(filas/2))*cols + (col-(cols/2))];
						d_matrix[(fila+(filas/2))*cols + (col-(cols/2))] = aux ;
					}
				}


		   }//del 2do for
		}//del 1er for
	}//del pragma

}

void normalizar_ifft (double complex *odata, int nx, int ny)
{

	int i,j;
	#pragma omp parallel shared(odata, nx, ny) private(i,j)
        {

		int tid = omp_get_thread_num();
		printf("debug: normalizar_ifft, thread_id:%d\n",tid);

		#pragma omp for schedule (static, CHUNKSIZE)
		for (i=0; i<nx; i++)
		{
		   for(j=0; j<ny; j++)
		   {    
		        //odata[i*nx+j][0] =  odata[i*nx+j][0] / ( double ) ( nx * nx ); //parte real
			    //odata[i*nx+j][1] =  odata[i*nx+j][1] / ( double ) ( nx * ny ); //parte imag
			    odata[i*nx+j] =  odata[i*nx+j] / ( double ) ( nx * ny ); //nro completo
		   	
		   }

		}

	}//del pragma  

}

//---------------------------------------------------------------------------------------------------------

//NUEVO INPLACE
//creacion de la funcion de referencia
void rfmS2 (double complex *S1, double *fslow, int nx, double *ffast, int ny){

	double d2PI = 6.2832;
	double res = 0.0;
	double root = 0.0;
	double complex _s1 = 0+0*I;

	double complex rfm = 0+0*I; //make_Complex(0.0f, 0.0f);

	int i,j;
	#pragma omp parallel shared(S1, fslow, ffast, nx, ny) private(i,j) firstprivate(d2PI,res, root,_s1,rfm)
        {

		int tid = omp_get_thread_num();
		printf("debug: rfmS2, thread_id:%d\n",tid);

		#pragma omp for schedule (static, CHUNKSIZE)
		for (i=0; i<nx; i++)
		{
		   for(j=0; j<ny; j++)
		   { 

			//--------------------genera la RFM al vuelo para ahorrar memoria ---------------

			root= (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i]) );

			res = dRfmC3 * sqrt( root  ) + dRfmC2 * (ffast[j]*ffast[j]);

			res = fmod(res, d2PI);

			rfm = cos(res)+sin(res)*I;
			//--------------------------------------------------------------------------------

			//out_s2[i*ny+j] = S1[i*ny+j] * ( rfm );//operador multiplicacion "* complejo"
			_s1 = S1[i*ny+j];

			S1[i*ny+j]  = _s1 * rfm ;//operador multiplicacion "* complejo"
		   }
		}
	}//del pragma
}

//--------------------------------------------------------


//creacion de la funcion de referencia
void delta_ffast (double *out_delta_ffast, double *fslow, int nx, double *ffast, int ny){

	double root = 0.0;
	double res =0.0;
	int i,j;
	#pragma omp parallel shared(out_delta_ffast, fslow, ffast, nx, ny) private(i,j) firstprivate(root, res)
        {

		int tid = omp_get_thread_num();
		printf("debug: delta_ffast, thread_id:%d\n",tid);

		#pragma omp for schedule (static, CHUNKSIZE)
		for (i=0; i<nx; i++)
		{
		   for(j=0; j<ny; j++)
		   { 
			//res = sqrt(  (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i])  ) - (dFc+ffast[j]);

			root = (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i])  );
			res = sqrt( root ) - (dFc+ffast[j]);

			//normaliza deltafasta a valores de indice mutiplicando por Nfast/Ffast
			//Nfast es ny
			res = res*(ny/dFfast);
			out_delta_ffast[i*ny+j]=res;
		   }

		}
	}//del pragma
}



double complex make_Complex(double re, double im)
{

	double complex n = re + im*I;
	return n;

}

//----------------------------------------------------------

void stolt (double complex *out_stolt_S3, double complex *S2, double *fslow, int filas, double *ffast, int cols){


	    //declaracion e inicializacion de variables para el calculo
	
	    double data_delta_ffast = 0.0f;


	    double complex valor_ant_3   = make_Complex(0.0f, 0.0f); //0+0*I;
	    double complex valor_ant_2   = make_Complex(0.0f, 0.0f);
	    double complex valor_ant_1   = make_Complex(0.0f, 0.0f);
        double complex valor_central = make_Complex(0.0f, 0.0f);
        double complex valor_pos_1   = make_Complex(0.0f, 0.0f);
        double complex valor_pos_2   = make_Complex(0.0f, 0.0f);
 	    double complex valor_pos_3   = make_Complex(0.0f, 0.0f);
        double complex valor_pos_4   = make_Complex(0.0f, 0.0f);

        
        int ent=0;
       	double dec= 0.0f;

       	double sinc_ant_3 = 0.0f;
       	double sinc_ant_2 = 0.0f;
       	double sinc_ant_1 = 0.0f;
       	double sinc_central= 0.0f;
     	double sinc_pos_1 = 0.0f;
       	double sinc_pos_2 = 0.0f;
     	double sinc_pos_3 = 0.0f;
       	double sinc_pos_4 = 0.0f;

	    int lim1 = Ni+dLim;    
	    int lim2 = cols-lim1;  

	    double root = 0.0;
	    double res =0.0;

	    int i,j;

        #pragma omp parallel shared(out_stolt_S3, S2, fslow, filas, ffast, cols) private(i,j) firstprivate(res, root, lim1, lim2, data_delta_ffast, valor_ant_3, valor_ant_2, valor_ant_1, valor_central, valor_pos_1, valor_pos_2, valor_pos_3, valor_pos_4, ent, dec, sinc_ant_3, sinc_ant_2, sinc_ant_1, sinc_central, sinc_pos_1, sinc_pos_2, sinc_pos_3, sinc_pos_4)
        {

		int tid = omp_get_thread_num();
		printf("debug: stolt(), thread_id:%d\n",tid);

		#pragma omp for schedule (static, CHUNKSIZE)
		for (i=0; i<filas; i++)
		{
		   for(j=lim1-1; j<=lim2-1; j++)
		   { 
	

			    //--------------genera delta_ffast al vuelo para ahorrar memoria---------------------

    			root = (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i])  );
    			res = sqrt( root ) - (dFc+ffast[j]);
    
    			//normaliza deltaffast a valores de indice mutiplicando por Nfast/Ffast
    			//Nfast es ny
    			res = res*(cols/dFfast);
    
    			//-----------------------------------------------------------------------------------
    
    			//valor double
    			data_delta_ffast = res; //deltafast[i * cols + j];
    
    			//toma parte enterea del double
    			ent= (int) data_delta_ffast;

			    //asignacion de double complex

			    valor_ant_3   = S2[i*cols + (j+ent-3)];
			    valor_ant_2   = S2[i*cols + (j+ent-2)];
			    valor_ant_1   = S2[i*cols + (j+ent-1)];
			    valor_central = S2[i*cols+(j+ent)];
			    valor_pos_1   = S2[i*cols + (j+ent+1)];
			    valor_pos_2   = S2[i*cols + (j+ent+2)];
			    valor_pos_3   = S2[i*cols + (j+ent+3)];
			    valor_pos_4   = S2[i*cols + (j+ent+4)];

	      
	       		dec= data_delta_ffast - ent; 

	 
	       		sinc_ant_3   = sinc_func(dec-3.0);
	       		sinc_ant_2   = sinc_func(dec-2.0);
	       		sinc_ant_1   = sinc_func(dec-1.0);

	       		sinc_central = sinc_func(dec); 

	       		sinc_pos_1   = sinc_func(dec+1.0);
	       		sinc_pos_2   = sinc_func(dec+2.0);
	       		sinc_pos_3   = sinc_func(dec+3.0);
	       		sinc_pos_4   = sinc_func(dec+4.0);


			    out_stolt_S3[i * cols + j] =      (valor_ant_3   *  sinc_ant_3) 
							+ (valor_ant_2   *  sinc_ant_2)  
							+ (valor_ant_1   *  sinc_ant_1) 
							+ (valor_central *  sinc_central) 
							+ (valor_pos_1   *  sinc_pos_1) 
							+ (valor_pos_2   *  sinc_pos_2)
							+ (valor_pos_3   *  sinc_pos_3) 
							+ (valor_pos_4   *  sinc_pos_4);

		    }//del 2do for	
		}//del 1er for
	}//del pragma //------------- hay una barrera implicita aqui -------------------

}


//------------------ Utils --------------------------


void initMat(double complex* h_data, int nx, int ny)
{
    	int i,j;
    	#pragma omp parallel shared(h_data, nx, ny) private(i,j)
        {

		int tid = omp_get_thread_num();
		printf("debug: initMat, thread_id:%d\n",tid);

		#pragma omp for schedule (static, CHUNKSIZE)
		for (i = 0; i < nx; i++)
			for(j = 0; j < ny; j++)
				h_data[(i * ny) + j]=0;
	}//del pragma

    return;
}


//IN: in, nx, ny
//OUT: max y min
//void getMaxMin_omp(double *in, int nx, int ny, double *max, double *min){
void getMaxMin_omp(double *in, int nx, int ny, double *max, double *min){

  	int i,j;
  	int k=0;

	double vmax[THREADS]={0};//de 0a3hilos
	double vmin[THREADS]={0};//de 0a3hilos

	double tmax, tmin;
	//zona paralela
      	#pragma omp parallel shared(in, nx, ny, vmax, vmin) private(i,j,tmax,tmin) firstprivate(k)
        {

		int tid = omp_get_thread_num();
		printf("debug: getMaxMin, thread_id:%d\n",tid);

		tmax=*max;
		tmin=*min;

		#pragma omp for schedule (static, CHUNKSIZE)
		for (i = 0; i < nx; i++)//filas
		{
			for ( j = 0; j <ny; j++)//cols
			{

			    k=(i*ny)+ j;

			    if (in[k] > tmax)
				    tmax=in[k];

			    if (in[k] < tmin)
				    tmin=in[k];


			}
		}
		vmax[tid]=tmax;
		vmin[tid]=tmin;

	}//del pragma: esta llave "{" una barrera implicita para los threads

	//fin zona paralela: falta la ultima parte del reduce:

	int t;
	*max=vmax[0];
	*min=vmin[0];

	for (t = 0; t < THREADS; t++)
	{
		if(vmax[t]> *max)
			*max=vmax[t];

		if(vmin[t]< *min)
			*min=vmin[t];
	}		
	

  return;

}


int linspace (double* vector, double minval, double maxval, int n)
{

	 if(n <2){
	    return 0;
	 }
	
	 int i = 0;
	 double step = (maxval-minval)/(floor((double)n) - 1.0);

	 for (i = 0; i < n; i++)
	 {
	    vector[i]= minval + i*step;

	 }

	 return 1;

}


int readMat2(char arch_re[], char arch_im[], complex* h_mat, int nx, int ny)
{
 
    int i,j;
    double re;
    double imag;


    FILE *pfile_re, *pfile_im;
    pfile_re = fopen(arch_re, "r");
    pfile_im = fopen(arch_im, "r");

    if (pfile_re == NULL)
    {
       printf("error al abrir archivo para lectura %s \n", arch_re);
       return (-1);
    }
    if (pfile_im == NULL)
    {
       printf("error al abrir archivo para lectura %s \n", arch_im);
       return (-1);
    }

    printf("leyendo archivo: %s \n", arch_re);
    printf("leyendo archivo: %s \n", arch_im);

    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
         fscanf(pfile_re, "%lf,",&re);
	 fscanf(pfile_im,"%lf,",&imag);

	 //h_mat[i*ny+j]= make_cuComplex(re, imag);
	 h_mat[i*ny+j]= re+imag*I;
       }

    }


   fclose(pfile_re);
   fclose(pfile_im);


   return 1;
}



int saveMat_Parts(complex *h_data, int nx, int ny, char re_arch[], char im_arch[])
{

    int i,j;

    FILE *pfile;
    pfile = fopen(re_arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", re_arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", re_arch);


    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
		if(j!=ny-1){

			   fprintf(pfile,"%lf,", creal(h_data[(i * ny) + j]));
			   
		}else{      //el ultimo caso no termina con ","

			   fprintf(pfile,"%lf", creal(h_data[(i * ny) + j]));
		
		}
       }
       fprintf(pfile,"\n");
    }

    fclose(pfile);

    //-----------------------------------

    pfile = fopen(im_arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", im_arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", im_arch);


    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
		if(j!=ny-1){

			   fprintf(pfile,"%lf,", cimag(h_data[(i * ny) + j]));
			   
		}else{      //el ultimo caso no termina con ","

			   fprintf(pfile,"%lf", cimag(h_data[(i * ny) + j]));
		
		}
       }
       fprintf(pfile,"\n");
    }

    fclose(pfile);

  return 0;


}



int saveVec(double *h_data, int nx, char arch[])
{

    int i;

    FILE *pfile;
    pfile = fopen(arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", arch);


    for (i = 0; i < nx; i++)
    {
	if(i!=nx-1){

	   fprintf(pfile,"%lf,", h_data[i]);

	}else{

	   fprintf(pfile,"%lf", h_data[i]);
	
       }
       fprintf(pfile,"\n");
    }

  fclose(pfile);

  return 0;


}






//--------------------------------main WK--------------------------------------------------------------

int main(int argc, char *argv[])
{

	//int i;

	printf("Nslow: %d, Nfast:%d, R0:%d\n, presione cualquier tecla", Nslow, Nfast, R0 );

	//--------omp -----------------
	omp_set_num_threads(THREADS);
	//--------fftw omp------------
	int fti = fftw_init_threads();
   	if(fti == 0) printf("error inicializando threads de fftw\n");
	fftw_plan_with_nthreads(THREADS);

	//----------------------------

	//leer S desde el disco
	//aplicar ifftshift 2D a S
	//aplicar fft2D a S
	//aplicar ifftshift 2D a S
	//obtener S1 para empezar a trabajar con el algoritmo


	double complex *h_S; 
	h_S =  (double complex *) malloc(  Nslow*Nfast * sizeof(double complex)); 
	char re_file[] = "re.txt"; 
	char im_file[] = "im.txt"; 


	if(LAB){	

		printf("Lectura datos crudos, omp: \n");
		getchar();
		readMat2(re_file, im_file, h_S, Nslow, Nfast);

	}

	if(TIME){

		struct timeval Lecturastart, Lecturaend;
		gettimeofday(&Lecturastart, NULL);


		readMat2(re_file, im_file, h_S, Nslow, Nfast);


		gettimeofday(&Lecturaend, NULL);
		printf("tiempo fftshift milisegundos:<%ld>\n", 
		((Lecturaend.tv_sec * 1000 + Lecturaend.tv_usec/1000) - (Lecturastart.tv_sec * 1000 + Lecturastart.tv_usec/1000)));

	}


	//------------------- iFFTSHIFT -------------solo matrices cuadradas-----------------

	//para el caso de matrices cuadradas se cumple que i-FFTSHIFT = FFTSHIFT, lo hace in-place

	//piso in-place a h_S!!

	if(LAB){
		printf("fftshift omp: \n");
		getchar();
		fftshift(h_S, Nslow, Nfast);
	}

	
	if(TIME){

		struct timeval shiftstart, shiftend;
		gettimeofday(&shiftstart, NULL);

		fftshift(h_S, Nslow, Nfast);

		gettimeofday(&shiftend, NULL);

		printf("tiempo fftshift milisegundos:<%ld>\n", 
		((shiftend.tv_sec * 1000 + shiftend.tv_usec/1000) - (shiftstart.tv_sec * 1000 + shiftstart.tv_usec/1000)));

	}


	//------------------- FFT2 -----------------------------------------------------------


	//FFT2

        double complex *h_out_S_fft = (double complex *) malloc(  Nslow*Nfast * sizeof(double complex)); 

	fftw_plan plan_forward; 

	//outplace
	//IN:h_S
	//OUT:h_out_S_fft
  	 
	if(LAB){	
		printf("FFTW directa omp: \n");
		getchar();

		plan_forward = fftw_plan_dft_2d ( Nslow, Nfast, h_S, h_out_S_fft, FFTW_FORWARD, FFTW_ESTIMATE );
		fftw_execute ( plan_forward );
	} 


	if(TIME){

		struct timeval fftwstart, fftwend;
		gettimeofday(&fftwstart, NULL);

		plan_forward = fftw_plan_dft_2d ( Nslow, Nfast, h_S, h_out_S_fft, FFTW_FORWARD, FFTW_ESTIMATE );
		fftw_execute ( plan_forward );

		gettimeofday(&fftwend, NULL);

		printf("tiempo _FFTW_ milisegundos:<%ld>\n", 
		((fftwend.tv_sec * 1000 + fftwend.tv_usec/1000) - (fftwstart.tv_sec * 1000 + fftwstart.tv_usec/1000)));

	}


	//---------------------limpieza ---------------------------------------------

	//TODO

	fftw_destroy_plan(plan_forward);
	free(h_S);


	//------------------- FFTSHIFT ------------solo matrices cuadradas-----------
	//lo hace in-place

	//IN  = h_out_S_fft
	//OUT = h_out_S_fft
	
	if(LAB){	

		printf("fftshift omp: \n");
		getchar();
		fftshift(h_out_S_fft, Nslow, Nfast);

	}

	 if(TIME){

		struct timeval shift2start, shift2end;
		gettimeofday(&shift2start, NULL);

		fftshift(h_out_S_fft, Nslow, Nfast);

		gettimeofday(&shift2end, NULL);

		printf("tiempo fftshift milisegundos:<%ld>\n", 
		((shift2end.tv_sec * 1000 + shift2end.tv_usec/1000) - (shift2start.tv_sec * 1000 + shift2start.tv_usec/1000)));

	}

	//----------------------------------------------------------------------------------------------

	//A PARTIR DE ACA DEBE CONTINUAR con  h_S1

	// h_S1 = h_out_S_fft

	//----------------- vectores ffast y fslow generadores de RFM -----------------------------------


    double *h_ffast  	    =  (double *) malloc( Nfast * sizeof(double));  
	double *h_fslow  	    =  (double *) malloc( Nslow * sizeof(double));  
        
        
	
	//vector fila
	//int minval =0;
	//int maxval =0;

	double minval =0;
	double maxval =0;
	int n =0;
	
	minval = -(Ffast/2);
	maxval =  (Ffast/2) - (Ffast/(double)Nfast);
	n =Nfast;


	if(LAB){	

		printf("linspace: \n");
		getchar();
		linspace(h_ffast,minval,maxval,n);

	}

 	if(TIME){

		struct timeval lin1start, lin1tend;
		gettimeofday(&lin1start, NULL);

		linspace(h_ffast,minval,maxval,n);

		gettimeofday(&lin1tend, NULL);

		printf("tiempo linspace milisegundos:<%ld>\n", 
		((lin1tend.tv_sec * 1000 + lin1tend.tv_usec/1000) - (lin1start.tv_sec * 1000 + lin1start.tv_usec/1000)));

	}

	
	//vector columna
	minval =-(Fslow/2); 
	maxval = (Fslow/2) - (Fslow/Nslow);
	n = Nslow ;

	if(LAB){	

		printf("linspace: \n");
		getchar();
		linspace(h_fslow,minval,maxval,n);

	}

	if(TIME){

		struct timeval lin2start, lin2end;
		gettimeofday(&lin2start, NULL);

		linspace(h_fslow,minval,maxval,n);

		gettimeofday(&lin2end, NULL);
		printf("tiempo linspace milisegundos:<%ld>\n", 
		((lin2end.tv_sec * 1000 + lin2end.tv_usec/1000) - (lin2start.tv_sec * 1000 + lin2start.tv_usec/1000)));

	}


	//------------------- ctes para RFM----------revisar Cte Fc !!!-----------------

	dRfmC1 = (C*C)/(4*V*V);
	dRfmC2 = PI/Ks;
	dRfmC3 = (4*PI*R0)/C;
	dFc=Fc;
	

	//---------llamada kernel rfm: construccion de la funcion de referencia -----------

	if(LAB){	

		printf("RFMS2 omp: \n");
		getchar();
		rfmS2 (h_out_S_fft, h_fslow, Nslow, h_ffast, Nfast);

	}
 	if(TIME){

		struct timeval rfms2start, rfms2end;
		gettimeofday(&rfms2start, NULL);

		//Atencion INPLACE!: 
		//S1:::h_out_S_fft
		//rfm:::h_fslow, Nslow, h_ffast, Nfast
		//S2 OUT:::h_out_S_fft
		rfmS2 (h_out_S_fft, h_fslow, Nslow, h_ffast, Nfast);

		gettimeofday(&rfms2end, NULL);

		printf("tiempo rfms2 milisegundos:<%ld>\n", 
		((rfms2end.tv_sec * 1000 + rfms2end.tv_usec/1000) - (rfms2start.tv_sec * 1000 + rfms2start.tv_usec/1000)));

	}


	//-------------------------constantes para Delta_fast---revisar---------------------------

	dFfast=Ffast;

    //------------------------llamada al kernel de delta_ffast --------------------------


	
	double *h_out_delta_fast =  (double *) malloc( Nslow*Nfast * sizeof(double)); 

	//IN=  d_fslow, Nslow, d_ffast, Nfast
	//OUT= d_out_delta_fast
	
	if(LAB){	

		printf("deltafast omp: \n");
		getchar();
		delta_ffast(h_out_delta_fast, h_fslow, Nslow, h_ffast, Nfast);

	}

 	if(TIME){

		struct timeval deltafaststart, deltafastend;
		gettimeofday(&deltafaststart, NULL);

		delta_ffast(h_out_delta_fast, h_fslow, Nslow, h_ffast, Nfast);

		gettimeofday(&deltafastend, NULL);

		printf("tiempo deltafast milisegundos:<%ld>\n", 
		((deltafastend.tv_sec * 1000 + deltafastend.tv_usec/1000) - (deltafaststart.tv_sec * 1000 + deltafaststart.tv_usec/1000)));

	}


	//**************************max y min*********************************************************

	double maxim2 = h_out_delta_fast[0];
	double minim2 = h_out_delta_fast[0];
	
	if(LAB){	

		printf("maxmin omp: \n");
		getchar();
		getMaxMin_omp(h_out_delta_fast, Nslow, Nfast, &maxim2, &minim2 );

	}

 	if(TIME){

		struct timeval maxminstart2, maxminend2;
		gettimeofday(&maxminstart2, NULL);

		getMaxMin_omp(h_out_delta_fast, Nslow, Nfast, &maxim2, &minim2 );

		gettimeofday(&maxminend2, NULL);

		printf("tiempo getMaxMin_OMP miliseg:<%ld>\n", 
		((maxminend2.tv_sec * 1000 + maxminend2.tv_usec/1000) - (maxminstart2.tv_sec * 1000 + maxminstart2.tv_usec/1000)));

	}


	int fixminim = (int) abs(minim2);
	int fixmaxim = (int) abs(maxim2);

        int fixmax_mm = MAX(fixmaxim,fixminim);
	
	dLim=fixmax_mm;

	//************************************************************************************


	//NUEVO: solo la usa para calcular el maximo y minimo, y la borra para salvar memoria: stoltnecesitara delta_ffast, pero la calculara al vuelo
	free(h_out_delta_fast);


	//***************************************** STOLT *********************************************

	//creacion de la matriz de salida de stolt

	//cantidad en columnas: desde  Ni+fixmax_mm   hasta  Nfast-Ni-fixmax_mm

	//en realidad seria num_cols = (Nfast-Ni-fixmax_mm) - (Ni+fixmax_mm) =  Nfast-2*(Ni+fixmax_mm)
    //y num_rows=: Nslow

	
	int dimx = Nslow;
	int dimy = Nfast;


	double complex *h_out_stolt_S3;
 	h_out_stolt_S3=  (double complex *) malloc( dimx*dimy * sizeof(double complex)); 

	
	if(LAB){	

		printf("initmat omp: \n");
		getchar();
		initMat(h_out_stolt_S3, dimx, dimy);

	}

 	if(TIME){

		struct timeval initstart, initend;
		gettimeofday(&initstart, NULL);

		initMat(h_out_stolt_S3, dimx, dimy);

		gettimeofday(&initend, NULL);
		printf("tiempo initMat milisegundos:<%ld>\n", 
		((initend.tv_sec * 1000 + initend.tv_usec/1000) - (initstart.tv_sec * 1000 + initstart.tv_usec/1000)));

	}

	//IN: double: h_out_delta_fast,  double complex: h_out_S2, int: Nslow, Nfast
	//OUT: double complex: h_out_stolt_S3
	
	if(LAB){	

		printf("stolt omp: \n");
		getchar();
		stolt(h_out_stolt_S3, h_out_S_fft, h_fslow, Nslow, h_ffast, Nfast);

	}

 	if(TIME){

		struct timeval stoltstart, stoltend;
		gettimeofday(&stoltstart, NULL);

		//NUEVO S2= h_out_S_fft
		//delta_Fast:::h_fslow, Nslow, h_ffast, Nfast
		stolt(h_out_stolt_S3, h_out_S_fft, h_fslow, Nslow, h_ffast, Nfast);

		gettimeofday(&stoltend, NULL);
		printf("tiempo stolt milisegundos:<%ld>\n", ((stoltend.tv_sec * 1000 + stoltend.tv_usec/1000) - (stoltstart.tv_sec * 1000 + stoltstart.tv_usec/1000)));

	}


	//*****************************************************************************************
	//NUEVO
	free(h_ffast);
        free(h_fslow);

	free(h_out_S_fft);//S2

	//----------------------------------i-FFTSHIFT------------------------------------------

	//para el caso de matrices cuadradas se cumple que i-FFTSHIFT = FFTSHIFT, lo hace in-place

	//piso in-place 
	//IN  = h_out_stolt_S3
	//OUT = h_out_stolt_S3
	
	if(LAB){	

		printf("fftshift omp: \n");
		getchar();
		fftshift(h_out_stolt_S3, Nslow, Nfast);

	}

 	if(TIME){

		struct timeval shift3start, shift3end;
		gettimeofday(&shift3start, NULL);

		fftshift(h_out_stolt_S3, Nslow, Nfast);

		gettimeofday(&shift3end, NULL);

		printf("tiempo fftshift milisegundos:<%ld>\n", 
		((shift3end.tv_sec * 1000 + shift3end.tv_usec/1000) - (shift3start.tv_sec * 1000 + shift3start.tv_usec/1000)));

	}
	
	//-----------------------------------iFFT----------------------------------------------

	//lo ejecuta in-place, piso de nuevo a h_out_stolt_S3 !!!!

	fftw_plan plan_backward;


	if(LAB){	

		printf("FFTW inversa omp: \n");
		getchar();

		plan_backward = fftw_plan_dft_2d ( Nslow, Nfast, h_out_stolt_S3, h_out_stolt_S3, FFTW_BACKWARD, FFTW_ESTIMATE );
  		fftw_execute ( plan_backward );
	}

 	if(TIME){

		struct timeval invFFTstart, invFFTend;
		gettimeofday(&invFFTstart, NULL);

		
		plan_backward = fftw_plan_dft_2d ( Nslow, Nfast, h_out_stolt_S3, h_out_stolt_S3, FFTW_BACKWARD, FFTW_ESTIMATE );
  		fftw_execute ( plan_backward );

		gettimeofday(&invFFTend, NULL);
		printf("tiempo InvFFTW milisegundos:<%ld>\n", 
		((invFFTend.tv_sec * 1000 + invFFTend.tv_usec/1000) - (invFFTstart.tv_sec * 1000 + invFFTstart.tv_usec/1000)));

	}

        //Normalizo la salida de i-FFT-- piso de nuevo a h_out_stolt_S3 !!!!
	
	if(LAB){	

		printf("normalizar omp: \n");
		getchar();
		normalizar_ifft(h_out_stolt_S3, Nslow, Nfast);

	}

 	if(TIME){

		struct timeval normstart, normend;
		gettimeofday(&normstart, NULL);

		normalizar_ifft(h_out_stolt_S3, Nslow, Nfast);

		gettimeofday(&normend, NULL);
		printf("tiempo normalizar milisegundos:<%ld>\n", ((normend.tv_sec * 1000 + normend.tv_usec/1000) - (normstart.tv_sec * 1000 + normstart.tv_usec/1000)));

	}



	//------------------------------------FFTSHIFT---------------------------------------------
        

	//obtener resultado final S4

	//para el caso de matrices cuadradas se cumple que i-FFTSHIFT = FFTSHIFT, lo hace in-place

	//piso in-place
	//IN  = d_out_stolt_S3
	//OUT = d_out_stolt_S3

	if(LAB){	

		printf("fftshift omp: \n");
		getchar();
		fftshift(h_out_stolt_S3, Nslow, Nfast);

	}

 	if(TIME){

		struct timeval shift4start, shift4end;
		gettimeofday(&shift4start, NULL);

		fftshift(h_out_stolt_S3, Nslow, Nfast);

		gettimeofday(&shift4end, NULL);
		printf("tiempo fftshift milisegundos:<%ld>\n", 
		((shift4end.tv_sec * 1000 + shift4end.tv_usec/1000) - (shift4start.tv_sec * 1000 + shift4start.tv_usec/1000)));

	}


	//-----------------------------------se guarda en disco S4------------------------------------
	
	if(WRITE)
	{

		char re_S4_file_debug[] = "re_S4_OMP6.txt";
		char im_S4_file_debug[] = "im_S4_OMP6.txt";

		if(LAB){	

			printf("Escritura datos focalizados, omp: \n");
			getchar();
			saveMat_Parts(h_out_stolt_S3,Nslow, Nfast, re_S4_file_debug, im_S4_file_debug );

		}

		if(TIME){

			struct timeval Escriturastart, Escrituraend;
			gettimeofday(&Escriturastart, NULL);

			saveMat_Parts(h_out_stolt_S3,Nslow, Nfast, re_S4_file_debug, im_S4_file_debug );

			gettimeofday(&Escrituraend, NULL);
			printf("tiempo escritura S4 milisegundos:<%ld>\n", 
			((Escrituraend.tv_sec * 1000 + Escrituraend.tv_usec/1000) - (Escriturastart.tv_sec * 1000 + Escriturastart.tv_usec/1000)));

		}

	}

	//------------------------------------Limpieza ----------------------------------------------


	free(h_out_stolt_S3);

	fftw_destroy_plan ( plan_backward );
	fftw_cleanup_threads();

	printf("WK:EOF:OK\n");

	return 0;

}


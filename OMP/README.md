# Algoritmo WK, version paralela para CPU Manycore, utilizando OPENMP

El algoritmo WK fue Creado en 1991 por Cafforio, Prati y Rocca. Es uno de los
algoritmos actuales más precisos comparado con RDA (Range Doppler Algorithm) y
CSA (Chirp Scaling Algorithm), realiza la focalización de la imagen SAR 
(randar de apertura sintética) trabajando íntegramente en el dominio bidimensional 
de las frecuencias y de allí su nombre, W:frecuencia en Rango y K: frecuencia en acimut. 
Sus principales ventajas es que puede manejar grandes aperturas sintéticas 
o elevados ángulos de Squint. 

## Descripción del algoritmo WK:

 Entrada: datos crudos simulados SAR

* ifftshit (version OpenMP)
* fft2D    (version OpenMP)
* iffshift (version OpenMP)
* compresion gruesa: RFM (version OpenMP)
* compresion diferencial: Interpolacion de Stolt (version OpenMP)
* ifft2D (version OpenMP)

Salida: Imagen focalizada

## Compilacion:

```
gcc wk_omp6_2018.c -lfftw3_omp -lfftw3 -lm -fopenmp -o wk_omp6_t4_6144 -DNslow=6144 -DNfast=6144 -DTHREADS=4 -DTIME=0 -DLAB=1 -DWRITE=0
```

## Autor

* **javier nicolas uranga: javiercba@gmail.com**

## Licencia

Este proyecto esta bajo lincencia GPLv3
//    wk_MPI_OMP.cu: 
//    Algoritmo WK - Version CPU para Cluster con MPI y Manycore con OpenMP

//    Copyright (C) 2018  javier nicolas uranga

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


//mail de contacto: javiercba@gmail.com


//mpicc  wk_MPI_OMP.c -lfftw3_omp -lfftw3 -lm -fopenmp -o wk_MPI_OMP -DNslow=6144 -DNfast=6144 -DTHREADS=4 -DTIME=1
//mpirun -np 2 --hostfile hostfilehome ./wk_MPI_OMP    

//*************************

#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>

#include <complex.h> // incluir despues de fftw3
#include <fftw3.h>

#include <math.h>
#include <omp.h>
#include <sys/time.h> 


#define Fc 9.4e9
#define C 3.0e8
#define V 250.0

#define R0 27000
#define Ks 10.0e12

//#define Fslow 600
#define Ffast 120.0e6
#define Fslow  6.0e2 

//#define Nslow 6144
//#define Nfast 6144

#ifndef Nslow
#define Nslow 1024
#endif

#ifndef Nfast
#define Nfast 1024
#endif

#define Ni 4

#define LAB 0

#ifndef TIME
#define TIME 0
#endif

#define PRINT 0
#define PI 3.1416

#define MAX(a,b)((a>b)?a:b)

//-------omp---------------

#define CHUNKSIZE 1

#ifndef THREADS
#define THREADS 4
#endif


//------------------MPI ---------------------------------------


MPI_Request  send_req, send_req2;
MPI_Status   recv_stat, recv_stat2; 
//, wait_stat, wait_stat2;

int rank, size;
 
int wk_mpi_barrier_counter=0;
//----------------------------------------------- kerneles ------------------------------------------------

double dFc;
double dRfmC3;
double dRfmC2;
double dRfmC1;

double dFfast;

int dLim;

double sinc_func(double x)
{
    return sin(PI*x+ 1e-32)/(PI*x+ 1e-32);
}



//---------------------------fft------------------------------------------------

//unicamente matrices cuadradas y pares
void fftshift(double complex *d_matrix, int filas, int cols)
{
	int fila, col;
	double complex aux;

		for (fila=0; fila<filas; fila++)
		{
		   for(col=0; col<cols; col++)
		   {
				if(fila<filas/2)
				{
					if(col<cols/2)
					{
						aux = d_matrix[fila*cols + col];
						d_matrix[fila*cols + col] = d_matrix[(fila+(filas/2))*cols + (col+(cols/2))];
						d_matrix[(fila+(filas/2))*cols + (col+(cols/2))] = aux;
					}
					else
					{
						aux = d_matrix[fila*cols + col];
						d_matrix[fila*cols + col] = d_matrix[(fila+(filas/2))*cols + (col-(cols/2))];
						d_matrix[(fila+(filas/2))*cols + (col-(cols/2))] = aux ;
					}
				}


		   }//del 2do for
		}//del 1er for


}

void normalizar_ifft (double complex *odata, int nx, int ny)
{

	int i,j;

		for (i=0; i<nx; i++)
		{
		   for(j=0; j<ny; j++)
		   {    
		        //odata[i*nx+j][0] =  odata[i*nx+j][0] / ( double ) ( nx * nx ); //parte real
			    //odata[i*nx+j][1] =  odata[i*nx+j][1] / ( double ) ( nx * ny ); //parte imag
			    odata[i*nx+j] =  odata[i*nx+j] / ( double ) ( nx * ny ); //nro completo
		   	
		   }

		}

}

//---------------------------------------------------------------------------------------------------------

//NUEVO INPLACE
//creacion de la funcion de referencia

void rfmS2 (double complex *S1, double *fslow, int nx, double *ffast, int ny){

	double d2PI = 6.2832;
	double res = 0.0;
	double root = 0.0;
	double complex _s1 = 0+0*I;

	double complex rfm = 0+0*I; //make_Complex(0.0f, 0.0f);

	int i,j;

		for (i=0; i<nx; i++)
		{
		   for(j=0; j<ny; j++)
		   { 

			//--------------------genera la RFM al vuelo para ahorrar memoria ---------------

			root= (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i]) );

			res = dRfmC3 * sqrt( root  ) + dRfmC2 * (ffast[j]*ffast[j]);

			res = fmod(res, d2PI);

			rfm = cos(res)+sin(res)*I;
			//--------------------------------------------------------------------------------

			//out_s2[i*ny+j] = S1[i*ny+j] * ( rfm );//operador multiplicacion "* complejo"
			_s1 = S1[i*ny+j];

			S1[i*ny+j]  = _s1 * rfm ;//operador multiplicacion "* complejo"
		   }
		}

}

//--------------------------------------------------------


//creacion de la funcion de referencia
void delta_ffast (double *out_delta_ffast, double *fslow, int nx, double *ffast, int ny){

	double root = 0.0;
	double res =0.0;
	int i,j;

		for (i=0; i<nx; i++)
		{
		   for(j=0; j<ny; j++)
		   { 
			//res = sqrt(  (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i])  ) - (dFc+ffast[j]);

			root = (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i])  );
			res = sqrt( root ) - (dFc+ffast[j]);

			//normaliza deltafasta a valores de indice mutiplicando por Nfast/Ffast
			//Nfast es ny
			res = res*(ny/dFfast);

			out_delta_ffast[i*ny+j]=res;
		   }

		}

}



double complex make_Complex(double re, double im)
{

	double complex n = re + im*I;
	return n;

}


void MPI_stolt (double complex *out_stolt_S3, double complex *S2, double *fslow, int filas, double *ffast, int cols){


	    printf("debug MPI: stolt(), rank_id:%d\n",rank);

	    //declaracion e inicializacion de variables para el calculo
	
	    double data_delta_ffast = 0.0f;


	    double complex valor_ant_3   = make_Complex(0.0f, 0.0f); //0+0*I;
	    double complex valor_ant_2   = make_Complex(0.0f, 0.0f);
	    double complex valor_ant_1   = make_Complex(0.0f, 0.0f);
        double complex valor_central = make_Complex(0.0f, 0.0f);
        double complex valor_pos_1   = make_Complex(0.0f, 0.0f);
        double complex valor_pos_2   = make_Complex(0.0f, 0.0f);
 	    double complex valor_pos_3   = make_Complex(0.0f, 0.0f);
        double complex valor_pos_4   = make_Complex(0.0f, 0.0f);

        
        int ent=0;
       	double dec= 0.0f;

       	double sinc_ant_3 = 0.0f;
       	double sinc_ant_2 = 0.0f;
       	double sinc_ant_1 = 0.0f;
       	double sinc_central= 0.0f;
     	double sinc_pos_1 = 0.0f;
       	double sinc_pos_2 = 0.0f;
     	double sinc_pos_3 = 0.0f;
       	double sinc_pos_4 = 0.0f;

    	int lim1 = Ni+dLim;    
    	int lim2 = cols-lim1;  
    
    	double root = 0.0;
    	double res =0.0;
    
    	int i,j;
    
    	// filas=Nslow, size: es el nro de nodos MPI
    	int filas_MPI_desde=(filas/size)*(rank);  
    	int filas_MPI_hasta=(filas/size)*(rank+1);
    
    	printf("debug MPI: stolt(), rank: %d, filas_MPI_desde:%d, filas_MPI_hasta:%d\n",rank, filas_MPI_desde, filas_MPI_hasta);


        #pragma omp parallel shared(out_stolt_S3, S2, fslow, filas, ffast, cols) private(i,j) firstprivate(filas_MPI_desde, filas_MPI_hasta, res, root, lim1, lim2, data_delta_ffast, valor_ant_3, valor_ant_2, valor_ant_1, valor_central, valor_pos_1, valor_pos_2, valor_pos_3, valor_pos_4, ent, dec, sinc_ant_3, sinc_ant_2, sinc_ant_1, sinc_central, sinc_pos_1, sinc_pos_2, sinc_pos_3, sinc_pos_4)
        {


		int tid = omp_get_thread_num();
		printf("debug: stolt(), thread_id:%d\n",tid);

		#pragma omp for schedule (static, CHUNKSIZE)
		for (i=filas_MPI_desde; i<filas_MPI_hasta; i++)
		{
		   for(j=lim1-1; j<=lim2-1; j++)
		   { 
	

    			//--------------genera delta_ffast al vuelo para ahorrar memoria---------------------
    
    			root = (double) ( (dFc+ffast[j])*(dFc+ffast[j]) - dRfmC1 * (fslow[i]*fslow[i])  );
    			res = sqrt( root ) - (dFc+ffast[j]);
    
    			//normaliza deltaffast a valores de indice mutiplicando por Nfast/Ffast
    			//Nfast es ny
    			res = res*(cols/dFfast);
    
    			//-----------------------------------------------------------------------------------
    
    			//valor double
    			data_delta_ffast = res; //deltafast[i * cols + j];
    
    			//toma parte enterea del double
    			ent= (int) data_delta_ffast;
    
    			//asignacion de double complex
    
    			valor_ant_3   = S2[i*cols + (j+ent-3)];
    			valor_ant_2   = S2[i*cols + (j+ent-2)];
    			valor_ant_1   = S2[i*cols + (j+ent-1)];
    			valor_central = S2[i*cols+(j+ent)];
    			valor_pos_1   = S2[i*cols + (j+ent+1)];
    			valor_pos_2   = S2[i*cols + (j+ent+2)];
    			valor_pos_3   = S2[i*cols + (j+ent+3)];
    			valor_pos_4   = S2[i*cols + (j+ent+4)];

	      
	       		dec= data_delta_ffast - ent; 

	 
	       		sinc_ant_3   = sinc_func(dec-3.0);
	       		sinc_ant_2   = sinc_func(dec-2.0);
	       		sinc_ant_1   = sinc_func(dec-1.0);

	       		sinc_central = sinc_func(dec); 

	       		sinc_pos_1   = sinc_func(dec+1.0);
	       		sinc_pos_2   = sinc_func(dec+2.0);
	       		sinc_pos_3   = sinc_func(dec+3.0);
	       		sinc_pos_4   = sinc_func(dec+4.0);


			    out_stolt_S3[i * cols + j] = (valor_ant_3   *  sinc_ant_3) 
							+ (valor_ant_2   *  sinc_ant_2)  
							+ (valor_ant_1   *  sinc_ant_1) 
							+ (valor_central *  sinc_central) 
							+ (valor_pos_1   *  sinc_pos_1) 
							+ (valor_pos_2   *  sinc_pos_2)
							+ (valor_pos_3   *  sinc_pos_3) 
							+ (valor_pos_4   *  sinc_pos_4);

		    }//del 2do for	
		}//del 1er for
	}//del pragma //------------- hay una barrera implicita aqui -------------------

}


//------------------ Utils --------------------------


void initMat(double complex* h_data, int nx, int ny)
{
    	int i,j;

		for (i = 0; i < nx; i++)
			for(j = 0; j < ny; j++)
				h_data[(i * ny) + j]=0;

        return;
}


void getMaxMin(double *in, int nx, int ny, double *max, double *min){

  		int i,j;
  		int k=0, l=0;

		for (i = 0; i < nx; i++)
		{
			for ( j = 0; j <= ny/2; j++)
			{
			    l= (i*ny)+ j;
		 	    k= (i*ny)+(ny-j-1);

			    if (in[l] > in[k])
			    {
				if (*min > in[k])
				    *min = in[k];
				if (*max< in[l])
				    *max = in[l];
			    }
			    else
			    {
				if (*min > in[l])
				    *min = in[l];
				if (*max< in[k])
				    *max = in[k];
			    }
			}
		}

  return;

}


int linspace (double* vector, double minval, double maxval, int n)
{


	 if(n <2){
	    return 0;
	 }
	
	 int i = 0;
	 double step = (maxval-minval)/(floor((double)n) - 1.0);

	 for (i = 0; i < n; i++)
	 {
	    vector[i]= minval + i*step;

	 }


	 return 1;

}


int readMat2(char arch_re[], char arch_im[], complex* h_mat, int nx, int ny)
{
 
    int i,j;
    double re;
    double imag;


    FILE *pfile_re, *pfile_im;
    pfile_re = fopen(arch_re, "r");
    pfile_im = fopen(arch_im, "r");

    if (pfile_re == NULL)
    {
       printf("error al abrir archivo para lectura %s \n", arch_re);
       return (-1);
    }
    if (pfile_im == NULL)
    {
       printf("error al abrir archivo para lectura %s \n", arch_im);
       return (-1);
    }

    printf("leyendo archivo: %s \n", arch_re);
    printf("leyendo archivo: %s \n", arch_im);

    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
         fscanf(pfile_re, "%lf,",&re);
	 fscanf(pfile_im,"%lf,",&imag);

	 //h_mat[i*ny+j]= make_cuComplex(re, imag);
	 h_mat[i*ny+j]= re+imag*I;
       }

    }


   fclose(pfile_re);
   fclose(pfile_im);


   return 1;
}



int saveMat_Parts(complex *h_data, int nx, int ny, char re_arch[], char im_arch[])
{

    int i,j;

    FILE *pfile;
    pfile = fopen(re_arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", re_arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", re_arch);


    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
		if(j!=ny-1){

			   fprintf(pfile,"%lf,", creal(h_data[(i * ny) + j]));
			   
		}else{      //el ultimo caso no termina con ","

			   fprintf(pfile,"%lf", creal(h_data[(i * ny) + j]));
		
		}
       }
       fprintf(pfile,"\n");
    }

    fclose(pfile);

    //-----------------------------------

    pfile = fopen(im_arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", im_arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", im_arch);


    for (i = 0; i < nx; i++)
    {
       for(j = 0; j < ny; j++)
       {
		if(j!=ny-1){

			   fprintf(pfile,"%lf,", cimag(h_data[(i * ny) + j]));
			   
		}else{      //el ultimo caso no termina con ","

			   fprintf(pfile,"%lf", cimag(h_data[(i * ny) + j]));
		
		}
       }
       fprintf(pfile,"\n");
    }

    fclose(pfile);

  return 0;


}



int saveVec(double *h_data, int nx, char arch[])
{

    int i;

    FILE *pfile;
    pfile = fopen(arch, "w");


    if (pfile == NULL)
    {
       printf("error al abrir archivo para escritura %s \n", arch);
       return (-1);
    }

    printf("Escribiendo archivo: %s \n", arch);


    for (i = 0; i < nx; i++)
    {
	if(i!=nx-1){

	   fprintf(pfile,"%lf,", h_data[i]);

	}else{

	   fprintf(pfile,"%lf", h_data[i]);
	
       }
       fprintf(pfile,"\n");
    }

  fclose(pfile);

  return 0;


}


//solo valido para cluster de 2 nodos
void wk_MPI_Barrier(int rank)
{

   MPI_Status   recv_stat_token;

   int token1=-1;

   if (rank==0){


	printf("barrera recibiendo token nodo 0\n");
	MPI_Recv(&token1, 1, MPI_INT, 1, 111+wk_mpi_barrier_counter, MPI_COMM_WORLD, &recv_stat_token); 

    }

    if (rank==1){


	printf("barrera enviando token nodo 1\n");
	MPI_Send(&token1, 1, MPI_INT, 0, 111+wk_mpi_barrier_counter, MPI_COMM_WORLD);

    }

   wk_mpi_barrier_counter = wk_mpi_barrier_counter + 1;
   return;
}

void wk_MPI_data_transfer(int rank, double complex *h_out_stolt_S3, int dslow, int dfast )
{

	int i,j;
	//tamaño del envio y recepcion
	int t= (dslow/2)*(dfast);
	double* h_MPI_buffer =  (double*) malloc( t * sizeof(double)); 
	double complex buf;

	if (rank==0){

		//--------------real

		printf( "nodo0, stolt, previo a la recepcion parte real:\n");
		//pisa en out_stolt_s3 desde la posicion t, pone t valores mas
		MPI_Recv(h_MPI_buffer, t, MPI_DOUBLE, 1, 10, MPI_COMM_WORLD, &recv_stat);

		printf( "nodo0, stolt, finalizacion de la recepcion parte real\n");

		
		for (i = dslow/2; i < dslow; i++)
		{
		       for(j = 0; j < dfast; j++)
		       {
				h_out_stolt_S3[(i * dfast) + j]= h_MPI_buffer[( (i-dslow/2) * dfast) + j]; //asigna un real puro
				
		       }

		}


		//--------------imag 


		printf( "nodo0, stolt,  previo a la recepcion parte imaginaria:\n");
		//pisa en out_stolt_s3 desde la posicion t, pone t valores mas
		MPI_Recv(h_MPI_buffer, t, MPI_DOUBLE, 1, 100, MPI_COMM_WORLD, &recv_stat2);

		printf( "nodo0, stolt, finalizacion de la recepcion parte imaginaria \n");

		
		for (i = dslow/2; i < dslow; i++)
		{
		       for(j = 0; j < dfast; j++)
		       {
			
				buf = h_MPI_buffer[( (i-dslow/2) * dfast) + j] * I; //imaginario puro

				h_out_stolt_S3[(i * dfast) + j] = h_out_stolt_S3[(i * dfast) + j] + buf; // +: suma compleja, le suma un imag puro, 
													 //  conserva la parte real
				
		       }

		}




  	}else if(rank==1){
 
		//------------real

		for (i = dslow/2; i < dslow; i++)
		{
		       for(j = 0; j < dfast; j++)
		       {
			
				h_MPI_buffer[( (i-dslow/2) * dfast) + j] =  creal(h_out_stolt_S3[(i * dfast) + j]);
				
		       }

		}


		printf( "nodo1, stolt enviando parte real \n");

		MPI_Send(h_MPI_buffer, t, MPI_DOUBLE, 0, 10, MPI_COMM_WORLD);

		printf( "nodo1, stolt envio finalizado parte real \n");


		//-------------imag 

		for (i = dslow/2; i < dslow; i++)
		{
		       for(j = 0; j < dfast; j++)
		       {
			
				h_MPI_buffer[( (i-dslow/2) * dfast) + j] =  cimag( h_out_stolt_S3[(i * dfast) + j] );
				
		       }

		}


		printf( "nodo1, stolt enviando parte imag  \n");

		MPI_Send(h_MPI_buffer, t, MPI_DOUBLE, 0, 100, MPI_COMM_WORLD);

		printf( "nodo1, stolt envio finalizado parte imaginaria \n");

  	}

	free(h_MPI_buffer);

	return;
}


//--------------------------------main WK--------------------------------------------------------------

int main(int argc, char **argv[])
{

	//int i,j;

	printf("Nslow: %d, Nfast:%d, R0:%d\n", Nslow, Nfast, R0 );

	//--------omp -----------------
	omp_set_num_threads(THREADS);

	//--------fftw omp------------
	//int fti = fftw_init_threads();
   	//if(fti == 0) printf("error inicializando threads de fftw\n");
	//fftw_plan_with_nthreads(THREADS);

	//-----------------------------------
	//------------MPI--------------------


  	MPI_Init (NULL, NULL);      			       /* starts MPI */
  	MPI_Comm_rank (MPI_COMM_WORLD, &rank);        /* get current process id */
  	MPI_Comm_size (MPI_COMM_WORLD, &size);        /* get number of processes */

	//----------------------------------

	//leer S desde el disco
	//aplicar ifftshift 2D a S
	//aplicar fft2D a S
	//aplicar ifftshift 2D a S
	//obtener S1 para empezar a trabajar con el algoritmo

    double complex *h_S =  (double complex *) malloc(  Nslow*Nfast * sizeof(double complex));  

	//Lectura de datos crudos
	char re_file[] = "re.txt"; 
	char im_file[] = "im.txt"; 


	readMat2(re_file, im_file, h_S, Nslow, Nfast);


	//------------------- iFFTSHIFT -------------solo matrices cuadradas-----------------

	//para el caso de matrices cuadradas se cumple que i-FFTSHIFT = FFTSHIFT, lo hace in-place

	printf("fftshift\n");
	//piso in-place a h_S!!
	fftshift(h_S, Nslow, Nfast);
	


	//------------------- FFT2 -----------------------------------------------------------


	//FFT2

    double complex *h_out_S_fft = (double complex *) malloc(  Nslow*Nfast * sizeof(double complex)); 

	fftw_plan plan_forward; 

	//outplace
	//IN:h_S
	//OUT:h_out_S_fft
  	plan_forward = fftw_plan_dft_2d ( Nslow, Nfast, h_S, h_out_S_fft, FFTW_FORWARD, FFTW_ESTIMATE );
	printf("fft\n");
	fftw_execute ( plan_forward );
	


	//---------------------limpieza ---------------------------------------------

	//TODO

	fftw_destroy_plan(plan_forward);
	free(h_S);


	//------------------- FFTSHIFT ------------solo matrices cuadradas-----------
	//lo hace in-place

	//IN  = h_out_S_fft
	//OUT = h_out_S_fft
	printf("fftshift2\n");
	fftshift(h_out_S_fft, Nslow, Nfast);
	

	//----------------- vectores ffast y fslow generadores de RFM -----------------------------------


    double *h_ffast  	    =  (double *) malloc( Nfast * sizeof(double));  
	double *h_fslow  	    =  (double *) malloc( Nslow * sizeof(double));  
        
        

	double minval =0;
	double maxval =0;
	int n =0;
	
	minval = -(Ffast/2);
	maxval =  (Ffast/2) - (Ffast/(double)Nfast);
	n =Nfast;

	linspace(h_ffast,minval,maxval,n);

	//vector columna
	minval =-(Fslow/2); 
	maxval = (Fslow/2) - (Fslow/Nslow);
	n = Nslow ;

	
	linspace(h_fslow,minval,maxval,n);
	


	//------------------- ctes para RFM----------revisar Cte Fc !!!-----------------

	dRfmC1 = (C*C)/(4*V*V);
	dRfmC2 = PI/Ks;
	dRfmC3 = (4*PI*R0)/C;
	dFc=Fc;
	
	//Atencion INPLACE!!!: 
	//S1:::h_out_S_fft
	//rfm:::h_fslow, Nslow, h_ffast, Nfast
	//S2 OUT:::h_out_S_fft
	printf("rfm\n");
	rfmS2 (h_out_S_fft, h_fslow, Nslow, h_ffast, Nfast);
	


	//-------------------------constantes para Delta_fast---revisar---------------------------

	dFfast=Ffast;

    //------------------------llamada al kernel de delta_ffast --------------------------


	
	double *h_out_delta_fast =  (double *) malloc( Nslow*Nfast * sizeof(double)); 
	printf("deltafast\n");
	delta_ffast(h_out_delta_fast, h_fslow, Nslow, h_ffast, Nfast);
	
	//**************************max y min*********************************************************


	double maxim2 = h_out_delta_fast[0];
	double minim2 = h_out_delta_fast[0];
	
	printf("maxmin\n");
	getMaxMin(h_out_delta_fast, Nslow, Nfast, &maxim2, &minim2 );

	int fixminim = (int) abs(minim2);
	int fixmaxim = (int) abs(maxim2);

    int fixmax_mm = MAX(fixmaxim,fixminim);
	
	dLim=fixmax_mm;

	//************************************************************************************

	//NUEVO: solo la usa para calcular el maximo y minimo, y la borra para salvar memoria: stoltnecesitara delta_ffast, pero la calculara al vuelo
	free(h_out_delta_fast);

	//***************************************** STOLT *********************************************

	int dimx = Nslow;
	int dimy = Nfast;


	double complex *h_out_stolt_S3;
 	h_out_stolt_S3=  (double complex *) malloc( dimx*dimy * sizeof(double complex)); 

	printf("initmat\n");
	initMat(h_out_stolt_S3, dimx, dimy);

	//--------------------------------MPI-----procesamiento stolt openmp------------------------------------

	wk_MPI_Barrier(rank);

	struct timeval start1, end1;
 	if(TIME){

		
		gettimeofday(&start1, NULL);
		printf("stolt nodo0\n");
		MPI_stolt(h_out_stolt_S3, h_out_S_fft, h_fslow, Nslow, h_ffast, Nfast);

		gettimeofday(&end1, NULL);
		
	}
	else
	{
		MPI_stolt(h_out_stolt_S3, h_out_S_fft, h_fslow, Nslow, h_ffast, Nfast);
	}

	wk_MPI_Barrier(rank);

	if(TIME && rank==0)
	{
		printf("nodo0 tiempo MPI_stolt_proc ms:<%ld>\n", ((end1.tv_sec * 1000 + end1.tv_usec/1000) - (start1.tv_sec * 1000 + start1.tv_usec/1000)));
	}
	if(TIME && rank==1)
	{
		printf("nodo1 tiempo MPI_stolt_proc ms:<%ld>\n", ((end1.tv_sec * 1000 + end1.tv_usec/1000) - (start1.tv_sec * 1000 + start1.tv_usec/1000)));
	}
	//-------------------------limpieza NUEVO -----------------------------------------------------------

	free(h_ffast);
	free(h_fslow);

	free(h_out_S_fft);//S2

	//************************************** transfer data MPI para stolt *******

	wk_MPI_Barrier(rank);

	struct timeval start2, end2;
 	if(TIME){

		
		gettimeofday(&start2, NULL);

		wk_MPI_data_transfer(rank, h_out_stolt_S3, Nslow, Nfast);

		gettimeofday(&end2, NULL);
		

	}
	else
	{
		wk_MPI_data_transfer(rank, h_out_stolt_S3, Nslow, Nfast);
	}

	wk_MPI_Barrier(rank);

	if(TIME && rank==0)
	{
		printf("nodo 0 tiempo MPI_transfer_stolt ms:<%ld>\n", ((end2.tv_sec * 1000 + end2.tv_usec/1000) - (start2.tv_sec * 1000 + start2.tv_usec/1000)));
	}

	if(TIME && rank==1)
	{
		printf("nodo 1 tiempo MPI_transfer_stolt ms:<%ld>\n", ((end2.tv_sec * 1000 + end2.tv_usec/1000) - (start2.tv_sec * 1000 + start2.tv_usec/1000)));
	}

	//*************************** fin MPI *********************************

	//****************** continua solo el nodo master  *******************
	
	if(rank==0){

			printf("fftshift\n");
			fftshift(h_out_stolt_S3, Nslow, Nfast);


			fftw_plan plan_backward;

			printf("i-fft\n");
			plan_backward = fftw_plan_dft_2d ( Nslow, Nfast, h_out_stolt_S3, h_out_stolt_S3, FFTW_BACKWARD, FFTW_ESTIMATE );
		  	fftw_execute ( plan_backward );

			fftw_destroy_plan ( plan_backward );

			printf("normalizar\n");
			normalizar_ifft(h_out_stolt_S3, Nslow, Nfast);
			
			printf("fftshift\n");
			fftshift(h_out_stolt_S3, Nslow, Nfast);
			
			//char re_S4_file_debug[] = "re_S4_MPI.txt";
			//char im_S4_file_debug[] = "im_S4_MPI.txt";
			//saveMat_Parts(h_out_stolt_S3,Nslow, Nfast, re_S4_file_debug, im_S4_file_debug );


	}//*************************************fin del nodo master******************
        

	free(h_out_stolt_S3);

	//fftw_cleanup_threads();

	printf("WK:finalizacion:OK\n");

	MPI_Finalize();
	return 0;

}

